// returns a list of coordinates normalized to the aspect ratio
function tesselate(isVertical, number) {
	let result = [];
	if (isVertical) {
		let rectangle = Math.sqrt(number);
		for (int i = 0; i < number; i++) {
			result.push({
				Math.trunc(i / rectangle) * 9,
				i % rectangle * 16
			});
		}
	} else {
		let rows = 1;
		let width, height;
		while (true) {
			width = Math.ceil(number / rows) * 9;
			height = Math.trunc(width * 9 / 16);
			if (height - (rows * 16) >= 16)
				rows--;
			else
				break;
		}
		let sum = 0, curRow = 0;
		for (int i = 0; i < number; i++) {
			sum += 9;
			if (sum > width) {
				sum = 0;
				curRow++;
			}
			result.push({
				sum,
				curRow * 16
			});
		}
	}
	return result;
}

