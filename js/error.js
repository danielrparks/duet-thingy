function rawError(str) {
	let elem = document.querySelector("#errortop");
	elem.innerHTML = str;
	elem.style.top = 0;
}

function reportError(what, event) {
	rawError("Error " + what + ": " + event);
	console.error(event);
}

window.onerror = function(message, source, lineno, colno, error) {
	message = message;
	rawError("Unknown error in " + source + ":" + lineno + ":" + colno + " — " + error + "<br>"
	+ "Please ensure you are using an at least relatively recent browser.");
}
