document.querySelectorAll(".play").forEach(e => e.addEventListener("click", playPause));
document.querySelector("#restart").addEventListener("click", restart);
document.querySelector("#record").addEventListener("click", record);

let playing = false;
let beginning = true;
let stream = null;
let streamvid = null;
let recorder = null;
let chunks = [];
let recording = true;

function updatePlayButton() {
	document.querySelectorAll(".play").forEach(e => e.innerText = playing ? "pause" : "play_circle");
}

function updateRecordButton() {
	document.querySelector("#record").disabled = recording;
}

async function playPause() {
	if (!playing) {
		if (beginning) {
			recording = false;
			await play();
		}
		else resume();
	} else pause();
	updatePlayButton();
}

async function record() {
	if (playing) {
		restart();
	}
	recording = true;
	await play();
	updatePlayButton();
	updateRecordButton();
}

function pause() {
	if (recording)
		recorder.pause();
	document.querySelectorAll(".countdown").forEach(x => x.style.display = "none");
	document.querySelectorAll(".vidcollection > video").forEach(x => x.pause());
	playing = false;
}

function resume() {
	if (recording)
		recorder.resume();
	document.querySelectorAll(".countdown").forEach(x => x.style.display = "none");
	document.querySelectorAll(".vidcollection > video").forEach(x => x.play());
	playing = true;
}

async function play() {
	if (recording) {
		try {
			stream = await navigator.mediaDevices.getUserMedia({
				audio: true,
				video: {
					width: 640,
					height: 360
				}
			});
		} catch (err) {
			reportError("opening camera", err);
			alert("Please allow access to your camera and microphone to record a duet.");
			return;
		}
		if (streamvid == null) {
			streamvid = document.createElement("video");
			streamvid.width = 640;
			streamvid.height = 360;
			streamvid.addEventListener("contextmenu", evt => evt.preventDefault());
			let e = document.querySelector(".vidcollection");
			e.innerHTML = "";
			e.appendChild(streamvid);
		}
		streamvid.srcObject = stream;
		streamvid.muted = true;
		recorder = new MediaRecorder(stream, { mimeType: "video/webm" });
		recorder.ondataavailable = e => chunks.push(e.data);
		recorder.start();
	}
	start(3, true);
	beginning = false;
	document.querySelector("#restart").disabled = false;
	playing = true;
}

function restart() {
	if (recording && recorder != null && recorder.state != "inactive") {
		recorder.onstop = null;
		recorder.ondataavailable = null;
		recorder.stop();
	}
	recording = false;
	updateRecordButton();
	if (playing) {
		pause();
		playing = false;
		updatePlayButton();
	}
	beginning = true;
	document.querySelectorAll(".vidcollection > video").forEach(x => x.currentTime = 0);
	chunks = [];
	document.querySelector("#restart").disabled = true;
}

function start(i, beg) {
	e = document.querySelectorAll(".countdown");
	if (beg) {
		e.forEach(x => {
			x.style.display = "initial";
			x.children[0].innerText = i;
		});
		document.querySelectorAll(".vidcollection > video").forEach(x => x.play());
		window.setTimeout(start, 1000, i - 1, false);
	} else if (i > 0) {
		e.forEach(x => x.children[0].innerText = i);
		window.setTimeout(start, 1000, i - 1, false);
	} else {
		e.forEach(x => x.style.display = "none");
	}
}

document.querySelector("#save").addEventListener("click", save);

function save() {
	if (recording && recorder != null && recorder.state != "inactive") {
		recorder.onstop = save2;
		recorder.stop();
	} else {
		save2();
	}
}

function save2() {
	save3().catch(function(err) {throw err;});
}

async function save3() {
	if (chunks.length) {
		curvw.push(new Blob(chunks, { type: "video/webm" }));
	}
	try {
		await saveFileAsyncHack();
	} catch (e) {
		reportError("saving file", e);
		return;
	}
	restart();
	updateVwHack();
}

function checkAPI(api) {
	if (window[api] === undefined) {
		window.stop();
		document.write("Your browser does not support the " + api + " API. Please update to a newer browser.");
		document.close();
	}
}
