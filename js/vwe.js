import { showOpenFilePicker, showSaveFilePicker } from "https://cdn.jsdelivr.net/npm/native-file-system-adapter@1.0.1/src/es6.js"

document.querySelector("#mainupload").addEventListener("click", uploadVw);

function uploadVw() {
	uploadVwAsync().then(vw => {
		curvw = vw;
		updateVw();
	}).catch(err => reportError("uploading file", err));
}

async function uploadVwAsync() {
	let curf = await showOpenFilePicker({
		excludeAcceptAllOption: true,
		types: [{
			description: "videowads",
			accept: {
				"application/octet-stream": [".videowad"]
			}
		}],
		accepts: [{
			extensions: ["videowad"]
		}]
	});
	if (Array.isArray(curf)) [curf] = curf;
	let curff = await curf.getFile();
	curfname = curff.name;
	return await vwFromFile(curff);
}

function updateVw() {
	let lelem = document.querySelector("#filelist");
	if (lelem.children.length) {
		for (let node of lelem.children[0].children) {
			URL.revokeObjectURL(node.href);
		}
	}
	lelem.innerHTML = "<ol start=0></ol>";
	lelem = lelem.children[0];
	for (let i = 0; i < curvw.length; i++) {
		let lelem2 = document.createElement("li");
		let aelem = document.createElement("a");
		aelem.download = i + ".webm";
		aelem.href = URL.createObjectURL(curvw[i]);
		aelem.innerText = "webm";
		lelem2.appendChild(aelem);
		lelem.appendChild(lelem2);
	}
}

document.querySelector("#secondaryupload").addEventListener("click", uploadVideo);

function uploadVideo() {
	uploadVideoAsync().then(vw => {
		updateVw();
	}).catch(err => reportError("uploading video", err));
}

async function uploadVideoAsync() {
	let handles = await showOpenFilePicker({
		multiple: true,
		excludeAcceptAllOption: true,
		types: [{
			description: "Video Files",
			accept: {
				"video/webm": [".webm"]
			}
		}],
		accepts: [{
			mimeTypes: ["video/webm"]
		}]
	});
	for (let h of handles) {
		curvw.push(await h.getFile());
	}
}

document.querySelector("#save").addEventListener("click", saveFile);

function saveFile() {
	saveFileAsync().catch(err => reportError("saving file", err));
}

async function saveFileAsync() {
	let output;
	if (curf) {
		output = await curf.createWritable();
	} else {
		output = await showSaveFilePicker({
			excludeAcceptAllOption: true,
			types: [{
				description: "videowads",
				accept: {
					"application/octet-stream": [".videowad"]
				}
			}],
			suggestedName: curfname ? curfname : "duet.videowad"
		});
		output = await output.createWritable();
	}
	await vwToFile(curvw, output);
	output.close();
}
