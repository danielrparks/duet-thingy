import { showOpenFilePicker, showSaveFilePicker } from "https://cdn.jsdelivr.net/npm/native-file-system-adapter@1.0.1/src/es6.js"

document.querySelector("#mainupload").addEventListener("click", uploadVw);

function uploadVw() {
	uploadVwAsync().then(vw => {
		curvw = vw;
		updateVw();
	}).catch(err => reportError("uploading file", err));
}

async function uploadVwAsync() {
	let curf = await showOpenFilePicker({
		excludeAcceptAllOption: true,
		types: [{
			description: "videowads",
			accept: {
				"application/octet-stream": [".videowad"]
			}
		}],
		accepts: [{
			extensions: ["videowad"]
		}]
	});
	if (Array.isArray(curf)) [curf] = curf;
	let curff = await curf.getFile();
	curfname = curff.name;
	return await vwFromFile(curff);
}

function updateVw() {
	let lelem = document.querySelector(".vidcollection");
	for (let node of lelem.children) {
		if (node.children.length && node.children[0].href)
			URL.revokeObjectURL(node.children[0].href);
	}
	lelem.innerHTML = "";
	for (let i = 0; i < curvw.length; i++) {
		let velem = document.createElement("video");
		let selem = document.createElement("source");
		/*
		velem.width = 640;
		velem.height = 360;
		*/
		velem.addEventListener("contextmenu", evt => evt.preventDefault());
		selem.src = URL.createObjectURL(curvw[i]);
		selem.type = "video/webm";
		velem.appendChild(selem);
		lelem.appendChild(velem);
	}
	if (typeof streamvid !== undefined) {
		streamvid = document.createElement("video");
		/*
		streamvid.width = 640;
		streamvid.height = 360;
		*/
		streamvid.addEventListener("contextmenu", evt => evt.preventDefault());
		lelem.appendChild(streamvid);
	}
}

async function saveFileAsync() {
	let output;
	if (curf) {
		output = await curf.createWritable();
	} else {
		output = await showSaveFilePicker({
			excludeAcceptAllOption: true,
			types: [{
				description: "videowads",
				accept: {
					"application/octet-stream": [".videowad"]
				}
			}],
			suggestedName: curfname ? curfname : "duet.videowad"
		});
		output = await output.createWritable();
	}
	await vwToFile(curvw, output);
	output.close();
}

saveFileAsyncHack = saveFileAsync;
updateVwHack = updateVw;
