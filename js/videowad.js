const magic = "https://danielrparks.gitlab.io/duetthingy"
const h1size = magic.length + 4;
let curvw = [];
let curf, curfname;
let saveFileAsyncHack, updateVwHack;

async function vwFromFile(file) {
	let header1 = file.slice(0, h1size);
	header1 = await header1.arrayBuffer();
	let enc = new TextDecoder("ascii");
	if (magic != enc.decode(new Uint8Array(header1.slice(0, magic.length)))) {
		throw "Incorrect file format";
	}
	let dv = new DataView(header1);
	let numChunks = dv.getUint32(magic.length, true);
	let header2 = file.slice(h1size, h1size + 8 * numChunks);
	header2 = await header2.arrayBuffer();
	dv = new DataView(header2);
	let chunkData = [];
	for (let i = 0; i < numChunks; i++) {
		chunkData.push(Number(dv.getBigUint64(i*8, true)));
	}
	blobs = [];
	for (let i = 0; i < numChunks - 1; i++) {
		blobs.push(file.slice(chunkData[i], chunkData[i+1]));
	}
	if (numChunks) {
		blobs.push(file.slice(chunkData[numChunks - 1]));
	}
	return blobs;
}

async function vwToFile(vw, writableStream) {
	let header = new ArrayBuffer(h1size + vw.length * 8);
	let enc = new TextEncoder("ascii");
	new Uint8Array(header).set(enc.encode(magic), 0);
	let dv = new DataView(header);
	dv.setUint32(magic.length, vw.length, true);
	let offset = h1size + vw.length * 8;
	for (let i = 0; i < vw.length; i++) {
		dv.setBigUint64(h1size + i * 8, BigInt(offset), true);
		offset += vw[i].size;
	}
	await writableStream.write(header);
	for (let i = 0; i < vw.length; i++) {
		await writableStream.write(vw[i]);
	}
}
