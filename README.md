# Duet Thingy

My music teacher told me that they used to have an app that let you record duets, but it switched to a subscription model. So I made this one, which is free because you have to shuffle your own files around 😛.
